import time
from datetime import datetime

def get_timespan(date):
	td = datetime.now() - date
	if td.days > 365:
		y = int(td.days/365)
		return '%d year%s' % (y, 's' if y > 1 else '')
	elif td.days > 0:
		d = td.days
		return '%d day%s' % (d, 's' if d > 1 else '')
	elif td.seconds > 3600:
		h = int(td.seconds/3600)
		return '%d hour%s' % (h, 's' if h > 1 else '')
	elif td.seconds > 60:
		m = int(td.seconds/60)
		return '%d minute%s' % (m, 's' if m > 1 else '')
	else:
		s = td.seconds
		return '%d second%s' % (s, 's' if s > 1 else '')

def parse_timespan(text):
	buffer = ''
	duration = 0

	for char in text:
		if char == 'd':
			duration += int(buffer) * 24 * 60 * 60
			buffer = ''
		elif char == 'h':
			duration += int(buffer) * 60 * 60
			buffer = ''
		elif char == 'm':
			duration += int(buffer) * 60
			buffer = ''
		elif char == 's':
			duration += int(buffer)
			buffer = ''
		else:
			buffer += char

	return duration

def unix_time(datetime):
	return int(time.mktime(datetime.timetuple()))

def format_ascii_irc(message):
	return message.replace('@errsep', '@b@c4::@o').replace('@nsep', '@b@c7::@o').replace('@sep', '@b@c3::@o').replace('@b', chr(2)).replace('@c', chr(3)).replace('@o', chr(15)).replace('@u', chr(31))
