import logging
import sqlite3
import sys
import threading
import time
from configparser import ConfigParser
from datetime import datetime

import importlib.machinery
import os
from pyrclib.bot import IRCBot

import utils
from database import Database

logger = logging.getLogger(__name__)


class CatBot(IRCBot):

    def __init__(self, config):
        self.nick = config.get('bot', 'nick')
        self.user = config.get('bot', 'user')
        self.realname = config.get('bot', 'gecos')
        self.db = Database(config.get('database', 'file'))
        IRCBot.__init__(self, self.nick, self.user, self.realname)
        self.master = {'nick': config.get('master', 'nick'),
                       'user': config.get('master', 'user'),
                       'host': config.get('master', 'host')}
        self.flags = {}  # ACL FLAGS?
        self.config = config
        self.commands = {}
        self.modules = {}
        self.hooks = {}

        for module in self.config.get('bot', 'autoload').split(','):
            self.load_module(module)

        self.starttime = datetime.now()

    def is_master(self, user):
        return (user.nick == self.master['nick'] and
                user.ident == self.master['user'] and
                user.host == self.master['host'])

    def on_privmsg(self, sender, channel, message):
        if 'privmsg' in self.hooks:
            for callback in self.hooks['privmsg']:
                try:
                    if callback['thread']:
                        thr = threading.Thread(target=callback['name'],
                                               args=(sender, channel, message))
                        thr.start()
                    else:
                        callback['name'](sender, channel, message)
                except Exception:
                    logger.exception('Error calling hook')

        if message[0] == '@':
            cmd, sep, arg = message[1:].partition(' ')
            if cmd == 'status':
                self.privmsg(channel, '[modules: {0}] [load: {1}] [uptime: {2}]'.format(
                    ', '.join(self.modules.keys()), str(os.getloadavg()),
                    utils.get_timespan(self.starttime)))

            for command, callback in self.commands.items():
                if cmd == command:
                    callback(sender, channel, arg)
                    return

            if not self.is_master(sender):
                return

            if cmd == 'reload' and not arg:
                if self.reload():
                    self.privmsg(channel, 'Reloaded configuration file')
                else:
                    self.privmsg(channel, 'Invalid configuration file!')

            if not arg:
                return

            if cmd == 'load':
                if self.load_module(arg):
                    self.privmsg(channel, 'Loaded module ' + arg)
            elif cmd == 'unload':
                if self.unload_module(arg):
                    self.privmsg(channel, 'Unloaded module ' + arg)
            elif cmd == 'reload':
                if self.reload_module(arg):
                    self.privmsg(channel, 'Reloaded module ' + arg)

    def open_dbconnection(self):
        conn = sqlite3.connect(self.config.get('database', 'path'))
        return conn

    def add_hook(self, hook, callback):
        if hook in self.hooks:
            self.hooks[hook].append(callback)
        else:
            self.hooks[hook] = [callback,]

    def add_command(self, cmd, callback):
        self.commands[cmd] = callback

    def load_module(self, modname):
        if modname in self.modules:
            logger.warning('Module %s is already loaded', modname)
            return False

        try:
            logger.info('Loading module %s...', modname)
            m = importlib.machinery.SourceFileLoader(
                'modules.' + modname, 'modules/mod_{0}.py'.format(modname)).load_module()
            module = getattr(m, 'mod_' + modname.capitalize())(self, config)
        except Exception:
            logger.exception('Failed loading module %s', modname)
            return False

        self.modules[modname] = module

        try:
            for hook, callback in module.hooks.items():
                self.add_hook(hook, callback)
            for cmd, callback in module.commands.items():
                self.add_command(cmd, callback)
            module.start()
            logger.info('Loaded module %s', modname)
            return True
        except Exception:
            logger.exception('Failed adding hooks/commands for module %s', modname)
            return False

    def unload_module(self, modname):
        if modname not in self.modules:
            logger.error('Module %s is not loaded', modname)
            return False

        mod = self.modules[modname]
        for hook, callback in mod.hooks.items():
            self.hooks[hook].remove(callback)
        for cmd, callback in mod.commands.items():
            del self.commands[cmd]

        try:
            mod.shutdown()
        except Exception:
            logger.exception('Module %s failed shutdown()', modname)
            return False

        sys.modules.pop('modules.' + modname)
        del self.modules[modname]
        logger.info('Unloaded module %s', modname)
        return True

    def reload_module(self, modname):
        if not self.reload():
            return False

        if self.unload_module(modname):
            if self.load_module(modname):
                return True

        return False

    def reload(self):
        try:
            self.config.read('config.ini')
            return True
        except Exception:
            logger.exception('Invalid configuration file')
            return False

    def on_CTCPREPLY_ping(self, sender, target, arg):
        if 'ctcpreply_ping' in self.hooks:
            for callback in self.hooks['ctcpreply_ping']:
                try:
                    if callback['thread']:
                        thr = threading.Thread(target=callback['name'], args=(sender, target, arg))
                        thr.start()
                    else:
                        callback['name'](sender, target, arg)
                except Exception:
                    logger.exception('Error calling hook')

    def on_notice(self, sender, channel, message):
        if 'notice' in self.hooks:
            for callback in self.hooks['notice']:
                try:
                    if callback['thread']:
                        thr = threading.Thread(target=callback['name'],
                                               args=(sender, channel, message))
                        thr.start()
                    else:
                        callback['name'](sender, channel, message)
                except Exception:
                    logger.exception('Error calling hook')

    def on_kick(self, sender, target, channel, reason):
        if 'kick' in self.hooks:
            for callback in self.hooks['kick']:
                try:
                    if callback['thread']:
                        thr = threading.Thread(target=callback['name'],
                                               args=(sender, target, channel, reason))
                        thr.start()
                    else:
                        callback['name'](sender, target, channel, reason)
                except Exception:
                    logger.exception('Error calling hook')


if __name__ == '__main__':
    config = ConfigParser()
    config.read('config.ini')
    catbot = CatBot(config)
    catbot.connect(config.get('connection', 'server'),
                   config.getint('connection', 'port'),
                   useSSL=config.getboolean('connection', 'ssl'))
    catbot.identify(config.get('connection', 'nspass'))
    time.sleep(1/2)
    ajoin = config.get('connection', 'ajoin')
    for chan in ajoin.split(','):
        pw = None
        if ' ' in chan:
            chan, pw = chan.split(' ', 1)
        catbot.join(chan, pw)
