from decimal import Decimal

from modules.module import Module


class mod_Bmi(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'bmi':
            sp = arg.split(' ')
            if len(sp) != 2:
                self.msg(channel, '@b[@bbmi@b]@b Syntax: !bmi height(cm) weight(kg).')
                return

            height = Decimal(sp[0])
            weight = Decimal(sp[1])
            bmi = round(weight / ((height / 100) ** 2),2)
            if bmi > 40:
                rating = '@c4@bObese Class III@o'
            elif bmi > 35:
                rating = '@c4Obese Class II@o'
            elif bmi > 30:
                rating = '@c4Obese Class I@o'
            elif bmi > 25:
                rating = '@c7Overweight@o'
            elif bmi > 18.5:
                rating = '@c3Normal@o'
            elif bmi > 16:
                rating = '@c10Underweight@o'
            else:
                rating = '@b@c10Severely underweight@o'

            self.msg(channel, '@b[@bbmi@b]@b {}m / {}kg @sep @bResult@b {} @sep @bRating@b {} @sep'.format(
                height/100, weight, bmi, rating))

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
