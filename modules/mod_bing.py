from urllib.parse import urlencode

from requests.exceptions import RequestException

from modules.feed import XmlFeed
from modules.module import Module


class mod_Bing(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.bing = Bing(self.config.get('translate', 'key'))
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 't' or cmd.lower() == 'translate':
            sp = arg.split(' ', 2)
            try:
                if len(sp) > 2:
                    source, target, text = sp
                    if source.lower() not in self.bing.languages or \
                            target.lower() not in self.bing.languages:
                        source = self.bing.detect_language(arg)
                        translation = self.bing.translate(arg)
                    else:
                        source = source.lower()
                        target = target.lower()
                        translation = self.bing.translate(text, source, target)
                else:
                    source = self.bing.detect_language(arg)
                    translation = self.bing.translate(arg)
            except RequestException as e:
                self.msg(channel, 'WARNING: Bing translate failed: %s' % e)
                return

            self.msg(channel, '[t] [from %s] %s' % (source, translation))

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True


class Bing(object):
    def __init__(self, key):
        self.API_KEY = key
        f = XmlFeed(
            'http://api.microsofttranslator.com/V2/Http.svc/GetLanguagesForTranslate?appId=' + self.API_KEY,
            namespaces={'ns': 'http://schemas.microsoft.com/2003/10/Serialization/Arrays'})
        self.languages = [lang.text() for lang in f.elements('//ns:string')]

    def translate(self, text, source=None, target='en'):
        url = 'http://api.microsofttranslator.com/V2/Http.svc/Translate?'
        url += urlencode({'text': text.encode('UTF-8'),
                          'to': target,
                          'appId': self.API_KEY})
        if source:
            url += '&from=' + source

        xml = XmlFeed(url, namespaces={'ns':'http://schemas.microsoft.com/2003/10/Serialization/'})
        return xml.text('//ns:string')

    def detect_language(self, text):
        url = 'http://api.microsofttranslator.com/V2/Http.svc/Detect?'
        url += urlencode({'text': text.encode('UTF-8'),
                          'appId': self.API_KEY})

        xml = XmlFeed(url, namespaces={'ns': 'http://schemas.microsoft.com/2003/10/Serialization/'})
        return xml.text('//ns:string')
