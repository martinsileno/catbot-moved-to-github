import sqlite3
from itertools import combinations, groupby

from modules.module import Module


class mod_Pokemon(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.pokedex = Pokedex(self.config.get('pokemon', 'dbfile'))
        self.user_cmds = {
            'pkmn': self.cmd_pokemon,
            'pktype': self.cmd_type,
            'nature': self.cmd_nature,
            # 'test': self.cmd_test,
            }
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() in self.user_cmds:
            self.user_cmds[cmd.lower()](sender, channel, arg)

    def cmd_pokemon(self, sender, channel, args):
        pk_stat, pk_type, pk_abil = self.pokedex.get_pokemon(args.lower())
        if not pk_stat:
            self.msg(channel, '@bError@b: pokemon not found')
            return

        pk_type = [t['name'] for t in pk_type]
        colors = [4, 7, 8, 10, 3, 13]
        stat_conv = {'HP': 'HP', 'Attack': 'Atk', 'Defense': 'Def', 'Special Attack': 'Sp.Atk',
                     'Special Defense': 'Sp.Def', 'Speed': 'Speed'}
        stats = ['@c{c}{name} {s[base_stat]}'.format(c=c, s=s, name=stat_conv[s['name']])
                 for c, s in zip(colors, pk_stat)]
        pk_abil = ', '.join(['%s%s' % (a['name'], ' (hidden)' if a['is_hidden'] == '1' else '')
                             for a in pk_abil])

        self.msg(channel,
                 '@sep @bName@b {name} @sep @bType@b {t} @sep @bBase stats@b {stats}@o @sep '
                 '@bXP@b {xp} @sep @bAbility@b {abil} @sep'.format(
                     name=pk_stat[0]['identifier'].title(),
                     t='/'.join(pk_type),
                     stats=' '.join(stats),
                     xp=pk_stat[0]['base_experience'],
                     abil=pk_abil
                 ))

    def cmd_type(self, sender, channel, args):
        offensive, defensive = self.pokedex.get_type(args.lower())
        if not defensive:
            self.msg(channel, '@bError@b: type not found')
            return

        type_name = offensive[0]['source']
        off_desc = {0: 'No dmg to', 50: 'Weak vs', 200: 'Strong vs'}
        def_desc = {0: 'Immune to', 50: 'Resistant to', 200: 'Weak to'}

        offensive = ' - '.join(
            '@b%s@b %s' % (off_desc[factor],
                           ', '.join(['{t[target]}'.format(t=t) for t in group]))
            for factor, group in groupby(offensive, lambda x: x['damage_factor']))
        defensive = ' - '.join(
            '@b%s@b %s' % (def_desc[factor],
                           ', '.join(['{t[source]}'.format(t=t) for t in group]))
            for factor, group in groupby(defensive, lambda x: x['damage_factor']))

        self.msg(channel,
                 '@sep @bType@b {tname} @sep @b@c4Offensive@c@b {offensive} @sep '
                 '@b@c3Defensive@c@b {defensive} @sep'.format(
                     tname = type_name,
                     offensive=offensive,
                     defensive=defensive
                 ))

    def cmd_nature(self, sender, channel, args):
        nature = self.pokedex.get_nature(args.lower())
        if not nature:
            self.msg(channel, '@bError@b: nature not found')
            return

        if nature['increased'] == nature['decreased']:
            self.msg(channel, '@sep @bNature@b {n[name]} @sep @bIncreases@b none @sep '
                              '@bDecreases@b none @sep'.format(n=nature))
        else:
            self.msg(channel, '@sep @bNature@b {n[name]} @sep @bIncreases@b {n[increased]} '
                              '@sep @bDecreases@b {n[decreased]} @sep'.format(n=nature))

    def cmd_test(self, sender, channel, args):
        if args == 'test':
            strong_vs, weak_vs, nodamage_to = self.pokedex.get_types()

        it = 0
        for size in range(1, 19):
            #it += 1
            #self.msg(channel, 'Calculating best cover of size %d' % size)
            bestcover = {'subset': {}, 'covers': set()}
            for subset in combinations(strong_vs.keys(), size):
                it += 1
                cover = set()
                for type in subset:
                    cover.update(strong_vs[type])
                if len(cover) > len(bestcover['covers']):
                    bestcover = {'subset': subset, 'covers': cover}

            is_complete = len(bestcover['covers']) == 18
            self.msg(channel, '[size %d] [complete: %s] [Best cover: %s] [Covers: %s]' %
                    (size, 'YES' if is_complete else 'NO', ', '.join(sorted(bestcover['subset'])), ', '.join(sorted(bestcover['covers']))))
            if is_complete:
                break
        self.msg(channel, 'Iterazioni: %d' % it)

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True


class Pokedex(object):
    def __init__(self, dbfile):
        self.conn = sqlite3.connect(dbfile, check_same_thread=False)
        self.types = None
        self.conn.row_factory = sqlite3.Row
        self.cur = self.conn.cursor()

    def get_types(self):
        if self.types:
            return self.types

        strong = {}
        weak = {}
        nodamage = {}

        self.cur.execute("""select src.type_id, src.name as 'source', damage_factor, dst.name as 'target' from types
        join type_names as src on types.id = src.type_id
        join type_efficacy on types.id = type_efficacy.damage_type_id
        join type_names as dst on type_efficacy.target_type_id = dst.type_id
        where src.local_language_id = 9 and dst.local_language_id = 9 and damage_factor != 100 order by damage_factor""")
        offensive = self.cur.fetchall()

        for match in offensive:
            if match['damage_factor'] > 100:
                d = strong
            elif match['damage_factor'] == 50:
                d = weak
            else:
                d = nodamage

            if match['source'] in d:
                d[match['source']].add(match['target'])
            else:
                d[match['source']] = {match['target']}

        return strong, weak, nodamage

    def get_pokemon(self, arg, by_id=False):
        self.cur.execute("""select pokemon.identifier, height, weight, base_experience, stat_names.name, base_stat, effort from pokemon
        join pokemon_stats on pokemon.id = pokemon_stats.pokemon_id
        join stats on pokemon_stats.stat_id = stats.id
        join stat_names on stats.id = stat_names.stat_id
        where pokemon.%s = ? and local_language_id = 9""" % ('id' if by_id else 'identifier'), (arg,)) # int arg?
        pkmn_stats = self.cur.fetchall()

        self.cur.execute("""select type_names.name from pokemon
        join pokemon_types on pokemon.id = pokemon_types.pokemon_id
        join types on pokemon_types.type_id = types.id
        join type_names on types.id = type_names.type_id
        where pokemon.%s = ? and local_language_id = 9 order by slot""" % ('id' if by_id else 'identifier'), (arg,))
        pkmn_types = self.cur.fetchall()

        self.cur.execute("""select is_hidden, ability_names.name from pokemon
        join pokemon_abilities on pokemon.id = pokemon_abilities.pokemon_id
        join abilities on pokemon_abilities.ability_id = abilities.id
        join ability_names on abilities.id = ability_names.ability_id
        where pokemon.%s = ? and local_language_id = 9 order by slot""" % ('id' if by_id else 'identifier'), (arg,))
        pkmn_abilities = self.cur.fetchall()

        return (pkmn_stats, pkmn_types, pkmn_abilities)

    def get_nature(self, arg, by_id=False):
        self.cur.execute("""select nature_names.name, dec.name as 'decreased', inc.name as 'increased' from natures
        join nature_names on natures.id = nature_names.nature_id
        join stat_names as dec on natures.decreased_stat_id = dec.stat_id
        join stat_names as inc on natures.increased_stat_id = inc.stat_id
        where natures.%s = ? and nature_names.local_language_id = 9 and dec.local_language_id = 9 and inc.local_language_id = 9""" % ('id' if by_id else 'identifier'), (arg,))
        nature = self.cur.fetchone()
        return nature

    def get_type(self, arg, by_id=False):
        self.cur.execute("""select src.type_id, src.name as 'source', damage_factor, dst.name as 'target' from types
        join type_names as src on types.id = src.type_id
        join type_efficacy on types.id = type_efficacy.damage_type_id
        join type_names as dst on type_efficacy.target_type_id = dst.type_id
        where src.local_language_id = 9 and types.%s = ? and dst.local_language_id = 9 and damage_factor != 100 order by damage_factor""" % ('id' if by_id else 'identifier'), (arg,))
        offensive = self.cur.fetchall()
        type_id = offensive[0]['type_id']

        self.cur.execute("""select src.name as 'source', damage_factor, dst.name as 'target' from types
        join type_names as src on types.id = src.type_id
        join type_efficacy on types.id = type_efficacy.damage_type_id
        join type_names as dst on type_efficacy.target_type_id = dst.type_id
        where src.local_language_id = 9 and dst.type_id = ? and dst.local_language_id = 9 and damage_factor != 100 order by damage_factor""", (type_id,))
        defensive = self.cur.fetchall()

        return offensive, defensive
