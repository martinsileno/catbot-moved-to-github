import logging
import urllib.parse
from datetime import datetime, timedelta
from sqlite3 import InterfaceError

import timeago
from requests.exceptions import RequestException

from modules.feed import get_json
from modules.module import Module

logger = logging.getLogger(__name__)


class mod_Weather(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.db.query('CREATE TABLE IF NOT EXISTS weather_users '
                      '(user TEXT NOT NULL, location TEXT NOT NULL, '
                      'PRIMARY KEY (user) ON CONFLICT ABORT)')
        self.API_KEY = self.config.get('weather', 'key')
        self.location_cache = {}  # nick -> location
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def api_request(self, url):
        logger.info('>>> WEATHER REQ: %s', url)
        resp = get_json(url)
        logger.info('<<< WEATHER REPLY: %s', resp)
        return resp

    def get_metar(self, location):
        url = f'https://avwx.rest/api/metar/{location}?options=&format=json&onfail=cache'
        js = self.api_request(url)

        return js

    def get_conditions(self, location):
        # request weather conditions for this location
        js = self.api_request(
            'https://api.openweathermap.org/data/2.5/weather?q={loc}&APPID={key}&units=metric'
            .format(key=self.API_KEY, loc=urllib.parse.quote(location.encode('UTF-8'))))

        return js

    def get_forecast(self, location):
        js = self.api_request(
            'https://api.openweathermap.org/data/2.5/forecast?q={loc}&APPID={key}&units=metric'
            .format(key=self.API_KEY, loc=urllib.parse.quote(location.encode('UTF-8'))))

        return js

    def get_location(self, sender, arg, channel):
        nick = None
        location = None

        if arg == '':
            nick = sender
        else:
            location = arg

        if nick and nick in self.location_cache:
            return self.location_cache[nick]
        elif nick:
            self.db.query('SELECT location FROM weather_users WHERE user = ?', nick)
            location = self.db.fetchone()[0]

        if not location:
            self.msg(channel, 'No location found linked to your nick. '
                              'To link one type: @b.register_location <location>@b')
        else:
            self.location_cache[nick] = location

        return location

    def set_location(self, user, location):
        self.db.query('INSERT OR IGNORE INTO weather_users (user, location) VALUES (?, ?)',
                      user, location)
        self.db.query('UPDATE weather_users SET location = ? WHERE user = ?', location, user)
        self.location_cache[user] = location

    def get_tempcolor(self, temp):
        if temp is None:
            return ''

        if temp < -20:
            code = '@c12@b'
        elif temp < -10:
            code = '@c12'
        elif temp < 0:
            code = '@c10'
        elif temp < 10:
            code = '@c09'
        elif temp < 20:
            code = '@c08'
        elif temp < 30:
            code = '@c07'
        elif temp < 35:
            code = '@c04'
        else:
            code = '@c04@b'

        return code

    def format_weather(self, code, message):
        return message.replace('@sep', '%s::@o' % code)

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        cmd = cmd.lower()
        if cmd == 'w' or cmd == 'weather':
            try:
                arg = self.get_location(sender.nick, arg, channel)
                if not arg:
                    return

                w = self.get_conditions(arg)
            except RequestException as e:
                self.errormsg(channel, e)
                return
            except InterfaceError as e:
                self.errormsg(channel, 'SQLite error (%s). Try again.' % e)
                return

            if w['cod'] == '404':
                self.errormsg(channel, f"{w['message']}")
                return

            code = self.get_tempcolor(w['main']['temp'])
            location_name = f"{w['name']}, {w['sys']['country']}"
            conditions = w['weather'][0]['main']
            temperature = round(w['main']['temp'], 1)
            felt_temperature = ''  # TODO
            dew_point = ''  # TODO
            pressure = w['main']['pressure']
            humidity = w['main']['humidity']
            visibility = w.get('visibility')
            visibility_str = f'{round(visibility / 1000, 1)}km' if visibility else 'N/A'
            wind_strength = round(w['wind']['speed'] * 3.6, 1)  # m/s to km/h
            wind_deg = w['wind'].get('deg')
            wind_direction_str = f' from {wind_deg}deg' if wind_deg else ''
            wind_gust = w['wind'].get('gust')
            wind_gust_str = f' (gusts {round(wind_gust * 3.6, 1)}km/h)' if wind_gust else ''
            last_update = timeago.format(
                datetime.utcfromtimestamp(w['dt']).strftime('%Y-%m-%d %H:%M:%S'))
            s = f'@sep @b{location_name}@b @sep @bConditions@b {conditions} @sep ' \
                f'@bTemperature@b {code}{temperature}C@o @sep ' \
                f'{felt_temperature}{dew_point}@bPressure@b {pressure}hPa @sep ' \
                f'@bHumidity@b {humidity}% @sep @bVisibility@b {visibility_str} @sep ' \
                f'@bWind@b {wind_strength}km/h{wind_direction_str}{wind_gust_str} @sep ' \
                f'@bLast update@b {last_update} @sep'
            self.msg(channel, self.format_weather(code, s))
        elif cmd == 'f' or cmd == 'forecast':
            try:
                arg = self.get_location(sender.nick, arg, channel)
                if not arg:
                    return

                w = self.get_forecast(arg)
            except RequestException as e:
                self.errormsg(channel, e)
                return
            except InterfaceError as e:
                self.errormsg(channel, 'SQLite error (%s). Try again.' % e)
                return

            if w['cod'] == '404':
                self.errormsg(channel, f"{w['message']}")
                return

            forecast_data = {
                'id': w['city']['id'],
                'city': w['city']['name'] + u', ',
                'country': w['city']['country'],
                'days': [],
            }

            # assemble and insert specific day data
            today = datetime.now()
            i = 0

            for day in w['list'][:4]:
                day_data = {
                    'name': (today + timedelta(days=i)).strftime('%A'),
                    'description': day['weather'][0]['description'].title().replace(' Is ', ' is '),
                    'min_c': int(day['main']['temp_min']),
                    'max_c': int(day['main']['temp_max']),
                }

                forecast_data['days'].append(day_data)
                i += 1

            # and set our data, with appropriate timeout
            # self.forecast_cache[api_data['city']['id']] = {
            #     'ts': dt.datetime.now(),
            #     'forecast': forecast_data,
            # }
            s = ' @sep '.join([
                f'@b{day["name"]}@b {day["description"]} @c04{day["max_c"]}C@c ' \
                f'@c10{day["min_c"]}C'
                for day in forecast_data['days']
            ])
            w_state = ''  # TODO
            self.msg(channel, f'@sep @b{forecast_data["city"]}{w_state}{forecast_data["country"]}@b @sep {s} @sep')
        elif cmd == 'regloc' or cmd == 'register_location':
            try:
                w = self.get_conditions(arg)
            except RequestException as e:
                self.errormsg(channel, e)
                return

            self.set_location(sender.nick, w['name'])
            self.msg(channel, '%s: registered location @b%s@b' %
                     (sender.nick, f"{w['name']}, {w['sys']['country']}"))
        elif cmd == 'metar':
            m = self.get_metar(arg)
            self.msg(channel, f'@bMETAR@b {m["raw"]}')

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
