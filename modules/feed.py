import json
from io import StringIO
from decimal import Decimal

import requests
from lxml import etree


class InputError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)


class HtmlFeed:
    def __init__(self, url, fake_ua=False):
        headers = {
            'user-agent': 'catbot',
        }
        self.response = requests.get(url, headers=headers, timeout=20)

    def html(self):
        return self.response.text

    def get_tree(self):
        parser = etree.HTMLParser()
        return etree.parse(StringIO(self.response.text), parser)


class PartialHtmlFeed:
    def __init__(self, url, bytes_range=32768):
        headers = {
            'user-agent': 'catbot',
            'Range': 'bytes=0-%d' % bytes_range,
        }
        self.response = requests.get(url, headers=headers, timeout=20)

    def html(self):
        return self.response.text

    def get_tree(self):
        parser = etree.HTMLParser()
        return etree.parse(StringIO(self.response.text), parser)


def get_json(url):
    feed = HtmlFeed(url)
    return json.load(StringIO(feed.html()))


class XmlFeed:
    def __init__(self, value, namespaces=None):
        self.namespaces = {} if namespaces is None else namespaces

        if isinstance(value, str):
            self._element = etree.parse(StringIO(HtmlFeed(value).html()))
        elif isinstance(value, etree._Element):
            self._element = value
        else:
            raise InputError('Invalid feed input type.')

    def elements(self, query):
        return [XmlFeed(x, self.namespaces)
                for x in self._element.xpath(query, namespaces=self.namespaces)]

    def text(self, query=None, default=None):
        if not query:
            result = self._element.text
        else:
            result = self._element.xpath(query, namespaces=self.namespaces)

        if len(result) == 0:
            value = default
        elif isinstance(result, str):
            value = result
        elif isinstance(result[0], str):
            value = result[0]
        elif len(result[0]) > 0:
            value = default
        elif result[0].text is None:
            value = default
        else:
            value = result[0].text.strip()

        if isinstance(value, str):
            try:
                value = value.encode('latin-1').decode('utf-8')
            except:
                pass

        return value

    def int(self, query, default=None):
        result = self.text(query, None)

        if result is None:
            return default

        try:
            return int(result)
        except:
            return default

    def decimal(self, query, default=None):
        result = self.text(query, None)

        if result is None:
            return default

        try:
            return Decimal(result)
        except:
            return default

    def bool(self, query, default=None):
        result = self.text(query, None)

        if result is None:
            return default

        if 'true' in result.lower() or result == '1':
            return True
        elif 'false' in result.lower() or result == '0':
            return False
        else:
            try:
                return int(result) > 0
            except:
                return default


class BrokenXmlFeed(XmlFeed):
    def __init__(self, url, namespaces=None):
        x = requests.get(url, timeout=20).text
        f = etree.fromstring(x)
        XmlFeed.__init__(self, f, namespaces)
