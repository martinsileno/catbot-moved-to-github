from modules.feed import HtmlFeed
from modules.module import Module


class mod_Euro2016(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'matches':
            if not arg:
                matches = self.get_matches()
                self.send_matchday(channel, matches[0])
            elif arg.lower() in ['t', 'tmr', 'tomorrow']:
                matches = self.get_matches()
                self.send_matchday(channel, matches[1])

    def send_matchday(self, channel, matchday):
        day = matchday['day']
        for match in matchday['matches']:
            odds = ' @sep '.join('@b{team[name]}@b {team[odd]}'.format(team=t)
                                 for t in match['odds'])
            self.msg(channel, '@sep @b{day}@b @sep At {m[time]} @sep {odds} @sep'.format(
                day=day, m=match, odds=odds))

    @staticmethod
    def get_matches():
        feed = HtmlFeed('http://www.oddschecker.com/football/euro-2016')
        tree = feed.get_tree()
        rows = tree.xpath(
            '/html/body/div[2]/div/div/div/div/div[2]/div/div[2]/section[1]/div/div/table/tbody/tr')

        matches = []  # [{'day': 'Friday 10th June 2016', 'matches': [{'time': '20:00', 'odds': {..
        block_dict = {}
        for row in rows:
            if row.attrib['class'] == 'date first':
                block_dict = {'day': row.xpath('td/p/span')[0].text, 'matches': []}
                matches.append(block_dict)
            elif row.attrib['class'] == 'match-on ':
                columns = row.xpath('td')
                hoursmin = columns[0].xpath('p')[0].text
                hours, minutes = hoursmin.split(':')
                hoursmin = '%d:%s' % (int(hours) + 1, minutes)
                odds = []
                for result in columns[1:4]:
                    odd = result.xpath('@data-best-odds')[0]
                    name = result.xpath('p[1]/span[1]/@data-name')[0]
                    odds.append({'name': name, 'odd': odd})
                block_dict['matches'].append({'time': hoursmin, 'odds': odds})

        return matches
