import random
from modules.module import Module


class mod_Factoids(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.db.query('CREATE TABLE IF NOT EXISTS factoids '
                      '(fact TEXT NOT NULL, definition TEXT NOT NULL, '
                      'UNIQUE (fact, definition) ON CONFLICT ABORT)')
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}
        self.poss_seps = [' is ', ' = ']

    def on_privmsg(self, sender, channel, args):
        if args.startswith('!learn'):
            cmd, sep, args = args.partition(' ')
            if not args or ' = ' not in args:
                self.msg(channel, '@b[@blearn@b]@b Usage: !learn <fact> = <definition>')
            else:
                fact, definition = args.split(' = ', 1)
                try:
                    self.add_factoid(fact, definition)
                except Exception:
                    self.msg(channel, '@b[@blearn@b]@b I already know that!')
        elif args.startswith('!forget'):
            cmd, sep, args = args.partition(' ')
            if not args or ' = ' not in args:
                self.msg(channel, '@b[@bforget@b]@b Usage: !forget <fact> = <definition>')
            else:
                fact, definition = args.split(' = ', 1)
                self.del_factoid(fact, definition)
        elif args.lower().startswith('what is'):
            fact = args[8:]
            self.print_factoid(channel, fact)
        elif args.startswith('??'):
            only_qmark = True
            for x in args:
                if x != '?':
                    only_qmark = False
            if only_qmark:
                return
            fact = args[2:].strip()
            self.print_factoid(channel, fact)
        elif args == '!countfacts':
            self.db.query('SELECT COUNT(*) FROM factoids')
            num = self.db.fetchone()
            self.msg(channel, '@bTotal@b: %s factoids' % num)
        else:
            for sep in self.poss_seps:
                if sep in args and 'what is' not in args:
                    fact, definition = args.split(sep, 1)
                    try:
                        self.add_factoid(fact, definition)
                    except Exception:
                        pass # duplicate key, ignore

    def add_factoid(self, fact, definition):
        self.db.query('INSERT INTO factoids (fact, definition) VALUES (?, ?)', fact, definition)

    def del_factoid(self, fact, definition):
        self.db.query('DELETE FROM factoids WHERE fact = ? AND definition = ?', fact, definition)

    def search_factoid(self, fact):
        self.db.query('SELECT definition FROM factoids WHERE fact = ?', fact)
        return self.db.fetchall()

    def print_factoid(self, channel, fact):
        definitions = self.search_factoid(fact)
        if not definitions:
            self.msg(channel, '@b[@bfactoids@b]@b Nothing found for @b%s@b' % fact)
        elif len(definitions) > 1:
            self.msg(channel,
                     '@b[@bfactoids@b] <{fact}>@b is {defn[0]} (and {other} other thing{s})'.format(
                         fact=fact,
                         defn=random.choice(definitions),
                         other=len(definitions) - 1,
                         s='s' if len(definitions) != 2 else ''))
        else:
            self.msg(channel,
                     '@b[@bfactoids@b] <{fact}>@b is {defn[0][0]}'.format(
                         fact=fact,
                         defn=definitions))

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
