from datetime import datetime

from modules.module import Module


class mod_Quotes(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.db.query('CREATE TABLE IF NOT EXISTS quotes '
                      '(id INTEGER PRIMARY KEY AUTOINCREMENT, '
                      'addedBy TEXT NOT NULL, '
                      'quote TEXT NOT NULL, '
                      'date DATETIME NOT NULL)')
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() != 'quote':
            return

        sub, sep, arg = arg.partition(' ')
        if sub.lower() == 'add':
            self.add(sender, channel, arg)
        elif sub.lower() == 'del':
            self.delete(sender, channel, arg)
        elif sub.lower() == 'read':
            self.read(sender, channel, arg)
        elif sub.lower() == 'search':
            self.search(sender, channel, arg)
        elif sub.lower() == 'random':
            self.random(sender, channel, arg)
        elif sub.lower() == 'stats':
            self.stats(sender, channel, arg)

    def add(self, sender, channel, arg):
        self.db.query('INSERT INTO quotes (addedBy, quote, date) VALUES (?, ?, ?)',
                      sender.nick, arg, datetime.now())
        self.msg(channel, '@b[@bquote@b]@b quote added')

    def delete(self, sender, channel, arg):
        self.db.query('SELECT id, addedBy, quote, date FROM quotes WHERE id = ?', arg)
        quote = self.db.fetchone()
        if not quote:
            self.msg(channel, '@b[@bquote@b]@b quote #{id} not found'.format(id=arg))
        else:
            self.db.query('DELETE FROM quotes WHERE id = ?', arg)
            self.msg(channel, '@b[@bquote@b]@b quote #{id} deleted'.format(id=arg))

    def read(self, sender, channel, arg):
        self.db.query('SELECT id, addedBy, quote, date FROM quotes WHERE id = ?', arg)
        quote = self.db.fetchone()
        if not quote:
            self.msg(channel, '@b[@bquote@b]@b quote #{id} not found'.format(id=arg))
        else:
            self.msg(channel, '@b[@bquote@b]@b #{id} added by {author} on {date}'.format(
                id=quote[0], author=quote[1], date=str(quote[3])[:19]))
            self.msg(channel, '@b[@bquote@b]@b {q}'.format(q=quote[2]))

    def search(self, sender, channel, arg):
        self.db.query('SELECT id, addedBy, quote, date FROM quotes WHERE quote LIKE ?',
                      '%' + arg + '%')
        result = self.db.fetchall()
        if len(result) == 1:
            quote = result[0]
            self.msg(channel, '@b[@bquote@b]@b #{id} added by {author} on {date}'.format(
                id=quote[0], author=quote[1], date=str(quote[3])[:19]))
            self.msg(channel, '@b[@bquote@b]@b {q}'.format(q=quote[2]))
        elif len(result) > 1:
            ids = ['#%d' % quote[0] for quote in result]
            self.msg(channel, '@b[@bquote@b]@b quotes found: {q}'.format(q=', '.join(ids)))
        else:
            self.msg(channel, '@b[@bquote@b]@b no quotes found')

    def random(self, sender, channel, arg):
        self.db.query('SELECT id, addedBy, quote, date FROM quotes ORDER BY RANDOM() LIMIT 1')
        quote = self.db.fetchone()
        self.msg(channel, '@b[@bquote@b]@b #{id} added by {author} on {date}'.format(
            id=quote[0], author=quote[1], date=str(quote[3])[:19]))
        self.msg(channel, '@b[@bquote@b]@b {q}'.format(q=quote[2]))

    def stats(self, sender, channel, arg):
        self.db.query('SELECT COUNT(*) FROM quotes')
        count = self.db.fetchone()[0]
        self.msg(channel, '@b[@bquote@b]@b total quotes: %d' % count)
