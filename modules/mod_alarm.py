import argparse
import time
import threading
from datetime import datetime, timedelta

from modules.module import Module


class mod_Alarm(Module):
    def __init__(self, bot, config):
        self.parser = argparse.ArgumentParser(prog='PROG', add_help=False)
        self.parser.add_argument('-s', '--seconds', action='store', type=int, default=0)
        self.parser.add_argument('-m', '--minutes', action='store', type=int, default=0)
        self.parser.add_argument('-h', '--hours', action='store', type=int, default=0)
        self.parser.add_argument('reason', nargs='+')

        self.alarms = []

        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'alarm':
            try:
                if not arg:
                    self.msg(channel, self.parser.print_usage())
                    return

                ns = self.parser.parse_args(arg.split(' '))
                td = timedelta(hours=ns.hours, minutes=ns.minutes, seconds=ns.seconds)
                reason = ' '.join(ns.reason)
                if self.add_alarm(sender.nick, channel, td, reason):
                    self.msg(channel, '[alarm] Alarm added. I\'ll remind you in {0}!'.format(
                        self.format_expire(td)))
            except:
                self.msg(channel, '[alarm] something was wrong')

    def add_alarm(self, nick, channel, td, reason=None):
        # sqlite
        self.alarms.append({
            'nick': nick,
            'channel': channel,
            'expire': datetime.now() + td,
            'reason': reason})
        return True

    def format_expire(self, td):
        sec = td.seconds
        res = ''
        if sec > 3600:
            h = int(sec / 3600)
            sec -= h * 3600
            res += '{0} hour{1}'.format(h, 's' if h != 1 else '')
        if sec > 60:
            m = int(sec / 60)
            sec -= m*3600
            res += '{0} minute{1}'.format(m, 's' if m != 1 else '')
        if sec > 0:
            res += '{0} second{1}'.format(sec, 's' if sec != 1 else '')
        return res

    def notify_alarm(self, alarm):
        self.msg(alarm['channel'], '[alarm] @b{0}@b: Time\'s up!{1}'.format(
            alarm['nick'], ' Reason: {0}'.format(alarm['reason'])
            if alarm['reason'] is not None else ''))

    def start(self):
        self.active = True
        self.monitor = Monitor(self)
        self.monitor.start()
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True


class Monitor(threading.Thread):
    def __init__(self, module):
        threading.Thread.__init__(self)
        self.module = module

    def run(self):
        while self.module.active:
            for alarm in self.module.alarms:
                if alarm['expire'] < datetime.now():
                    self.module.notify_alarm(alarm)
                    self.module.alarms.remove(alarm)
            time.sleep(10)
