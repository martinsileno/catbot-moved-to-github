import io
import re

import lxml.html
import requests

from modules.module import Module

# AMP_REGEX = re.compile(
#     r"http(|s)://(|.+\.)(?<!(developer)\.)google\.[a-z.]+/amp/(?:s/)?(.*)", re.IGNORECASE | re.MULTILINE
#     #r"google[a-z.]+/amp/(?:s/)?(.*)", re.IGNORECASE | re.MULTILINE
# )  # https://regex101.com/r/92WOyk/1
AMP_REGEX = re.compile(r'https?://.+\.ampproject.org[^\s]+')


class mod_Deampify(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}

    def on_privmsg(self, sender, channel, args):
        matched = mod_Deampify.is_amp_url(args)
        if not matched:
            return

        canonical_url = mod_Deampify.extract_canonical_url(matched)
        self.msg(channel, f'@c4DeAMPified URL@c: {canonical_url}')

    @staticmethod
    def is_amp_url(text):
        matches = re.findall(AMP_REGEX, text)

        if len(matches) == 1:
            return matches[0]
        else:
            return False

    @staticmethod
    def extract_canonical_url(amp_url):
        cont = requests.get(amp_url).text
        tree = lxml.html.parse(io.StringIO(cont))
        canonical_url = tree.xpath('/html/head/link[@rel="canonical"]/@href')
        if canonical_url and canonical_url[0] is not None:
            return canonical_url[0]
