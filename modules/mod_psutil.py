import time

import psutil
from decimal import Decimal
from datetime import datetime, timedelta

from modules.module import Module


class mod_Psutil(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def convert_bytes(self, bytes):
        bytes = float(bytes)
        if bytes >= 1099511627776:
            terabytes = bytes / 1099511627776
            size = '%.2fT' % terabytes
        elif bytes >= 1073741824:
            gigabytes = bytes / 1073741824
            size = '%.2fG' % gigabytes
        elif bytes >= 1048576:
            megabytes = bytes / 1048576
            size = '%.2fM' % megabytes
        elif bytes >= 1024:
            kilobytes = bytes / 1024
            size = '%.2fK' % kilobytes
        else:
            size = '%.2fb' % bytes
        return size

    def on_privmsg(self, sender, channel, args):
        if args == '!memstat':
            total, free, used, cached, buffered = self.get_memusage()
            self.msg(channel,
                     '@b[@bmemstat@b]@b @bTotal@b {total} @c4@b[used]@o {used} @c7@b[buffered]@o '
                     '{buffered} @c10@b[cached]@o {cached} @c3@b[free]@o {free}'.format(
                         used=used,
                         total=total,
                         free=free,
                         cached=cached,
                         buffered=buffered))
        elif args == '!memusage':
            total, free, used, cached, buffered = self.get_memusage()
            m = '@b[@bmemusage@b]@b @c15@b[@c4'
            m += 'o' * round(Decimal(used[:-1])/10) + '@c15 | @c7'
            m += 'o' * round(Decimal(buffered[:-1])/10) + '@c10'
            m += 'o' * round(Decimal(cached[:-1])/10) + '@c3'
            m += 'o' * round(Decimal(free[:-1])/10) + '@c15]@o '
            m += '@c15[@c04{} @c07{} @c10{} @c03{}@c15]@o'.format(used, buffered, cached, free)
            self.msg(channel, m)
        elif args.startswith('!pstat'):
            cmd, sep, args = args.partition(' ')
            if not args:
                self.msg(channel, '@b[@berror@b]@b No process ID specified.')
                return

            try:
                pid = int(args)
                p = psutil.Process(pid)
            except psutil.NoSuchProcess:
                self.msg(channel, '@b[@berror@b]@b Invalid process ID specified.')
                return

            ct = p.cpu_times()
            meminfo = p.memory_info()
            self.msg(channel,
                     '@b[@bpstat@b]@b @bProcess@b {name} [{owner}] @sep @bStatus@b {status} @sep '
                     '@bCPU@b {cpu}% @sep @bCPU Time@b {cput} ({user} user / {sys} system) @sep '
                     '@bMemory@b {mem} [{pmem}%] / VM Size {vmem} @sep '
                     '@bStart date@b {date:%Y-%m-%d %H:%M} @sep'.format(
                         name=p.name(),
                         owner=p.username(),
                         status=p.status(),
                         cpu=p.cpu_percent(interval=1.0),
                         cput=str(timedelta(seconds=ct.user + ct.system)).split('.')[0],
                         user=str(timedelta(seconds=ct.user)).split('.')[0],
                         sys=str(timedelta(seconds=ct.system)).split('.')[0],
                         mem=self.convert_bytes(meminfo.rss),
                         pmem=round(p.memory_percent(), 2),
                         vmem=self.convert_bytes(meminfo.vms),
                         date=datetime.fromtimestamp(p.create_time())
                     ))
        elif args == '!diskstat':
            dstat = psutil.disk_usage('/')
            self.msg(channel,
                     '@b[@bdiskstat@b]@b @bFree@b {free} / {total} '
                     '@bTotal@b @sep {dstat.percent}% used @sep'.format(
                         dstat=dstat,
                         free=self.convert_bytes(dstat.free),
                         total=self.convert_bytes(dstat.total)
                     ))
        elif args.startswith('!pgrep'):
            cmd, sep, args = args.partition(' ')
            res = []
            for p in psutil.process_iter():
                if p.name() == args.lower():
                    res.append(p)

            if res:
                self.msg(channel, '@b[@bpgrep {pid}@b]@b {pidlist} @sep'.format(
                    pid=args,
                    pidlist=' @sep '.join(['@b{}@b [{}]'.format(p.pid, p.username()) for p in res])
                ))
                if len(res) == 1:
                    self.on_privmsg(sender, channel, '!pstat %d' % res[0].pid)
            else:
                self.msg(channel, '@b[@bpgrep {pid}@b]@b No process found'.format(pid=args))
        elif args.lower() == '!time':
            self.msg(channel, '@b[@btime@b]@b {}'.format(
                time.strftime('%a %d %b %Y - %H:%M:%S %Z', time.localtime())
            ))

    def get_memusage(self):
        memory = psutil.virtual_memory()
        total = self.convert_bytes(memory.total)
        free = self.convert_bytes(memory.free)
        used = self.convert_bytes(memory.used)
        cached = self.convert_bytes(memory.cached)
        buffered = self.convert_bytes(memory.buffers)
        return total, free, used, cached, buffered

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
