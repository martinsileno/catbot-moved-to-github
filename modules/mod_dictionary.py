from requests.exceptions import RequestException
from wordnik.swagger import ApiClient
from wordnik.WordApi import WordApi

from modules.module import Module


class mod_Dictionary(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}

        api_key = self.bot.config.get('wordnik', 'key')
        api_url = self.bot.config.get('wordnik', 'url')
        self.wordnik = WordApi(ApiClient(apiKey=api_key, apiServer=api_url))
        self.last_def = {}

    def get_definition(self, word):
        if word in self.last_def:
            return self.last_def[word]

        defs = self.wordnik.getDefinitions(word, sourceDictionaries='ahd', useCanonical=True,
                                           includeTags=False)

        self.last_def = {word: defs}
        return defs

    def on_privmsg(self, sender, channel, args):
        if not args.startswith('!') and not args.startswith('.'):
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() != 'dict':
            return

        if not arg:
            self.errormsg(channel, '@b[@bdictionary@b]@b Usage: .dict <word> [def_number]')
            return

        arg, sep, number = arg.partition(' ')
        try:
            results = self.get_definition(arg)
        except RequestException as e:
            self.errormsg(channel, e)
            return

        if not results:
            self.msg(channel, '@b[@bdictionary@b]@b Nothing found')
            return

        if number:
            try:
                number = int(number)
            except ValueError:
                self.errormsg(channel, '@b[@bdictionary@b]@b Invalid definition number')
                return

        if number:
            if number - 1 < 0 or number - 1 >= len(results):
                self.errormsg(channel,
                              'definition number out of range: only %d definitions found.' %
                              len(results))
                return

            result = results[number - 1]
            self.msg(channel,
                     '@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
                         res=result, num=number, tot=len(results)))
        else:
            for n, res in enumerate(results, 1):
                self.msg(channel,
                         '@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
                             res=res, num=n, tot=len(results)))
                if n == 4:
                    break
