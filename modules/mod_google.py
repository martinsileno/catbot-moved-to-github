import re
from urllib.parse import urlencode
from html.entities import name2codepoint

from requests.exceptions import RequestException

from modules.feed import HtmlFeed, get_json
from modules.module import Module
from modules.utils import format_hms, format_thousand
name2codepoint['#39'] = 39


class mod_Google(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        key = config.get('google', 'key')
        engine_id = config.get('google', 'engine_id')
        self.google = Google(key, engine_id)
        self.embedded_re = re.compile(r'{(.+?)}')
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def unescape(self, s):
        """unescape HTML code refs; c.f. http://wiki.python.org/moin/EscapingHtml"""
        return re.sub('&(%s);' % '|'.join(name2codepoint), lambda m: chr(name2codepoint[m.group(1)]), s)

    def _google_search(self, channel, arg):
        try:
            result = self.google.search(arg)
        except RequestException as e:
            self.errormsg(channel, e)
            return

        if result['queries']['request'][0]['totalResults'] == 0:
            self.msg(channel, '[Google] No results found')
            return

        json = result['items'][0]
        self.msg(channel, '[Google] @b%(title)s@b (@u%(url)s@u)' % {
            'title': self.unescape(json['title']),
            'url': json['link']})
        self.msg(channel, '[Google] @bDescription@b: %s' %
                 self.unescape(json['snippet']).replace('<b>', '@b')
                 .replace('</b>', '@b').replace('\n', ''))

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            if '{' in args and '}' in args:
                groups = self.embedded_re.findall(args)
                if groups:
                    if len(groups) == 1:
                        self._google_search(channel, groups[0])
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd == 'g' or cmd == 'google':
            self._google_search(channel, arg)
        elif cmd == 'cc' or cmd == 'calc':
            try:
                result = self.google.calc(arg)
            except RequestException as e:
                self.errormsg(channel, e)
                return

            if not result:
                self.errormsg(channel, 'invalid expression')
            else:
                self.msg(channel, '[calc] %s' % result)
        elif cmd == 'yt' or cmd == 'youtube':
            try:
                res = self.google.yt_search(arg)
            except RequestException as e:
                self.errormsg(channel, e)
                return

            if not res:
                self.msg(channel, '[YouTube] No results found')
                return

            self.msg(channel,
                     '@sep @bYouTube@b %(title)s @sep @bURL@b %(url)s (%(duration)s) @sep '
                     '@bViews@b %(views)s @sep @bRating@b @c3@b[+]@b %(liked)s likes @c4@b[-]@b '
                     '%(disliked)s dislikes @sep' % {
                         'title': res['title'],
                         'url': 'http://www.youtube.com/watch?v=' + res['id'],
                         'duration': '%s' % format_hms(res['duration']),
                         'views': format_thousand(res['view_count']),
                         'liked': format_thousand(res['liked']) if res['liked'] else 0,
                         'disliked': format_thousand(res['disliked']) if res['disliked'] else 0
                     })

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True


class Google(object):
    def __init__(self, key, engine_id):
        self.api_key = key
        self.engine_id = engine_id

    def calc(self, expr):
        url = 'https://www.google.it/search?'
        url += urlencode({'q': expr.encode('UTF-8')})
        page = HtmlFeed(url, fake_ua=True).get_tree()
        result = None
        if page.xpath('//*[@id="cwles"]') and page.xpath('//*[@id="cwos"]'):
            # CALC (3 + 2 = 5)
            result = "%s %s" % (page.xpath('//*[@id="cwles"]')[0].text.strip(),
                                round(float(page.xpath('//*[@id="cwos"]')[0].text.strip()), 4))
        return result

    def search(self, query, userip=None):
        url = 'https://www.googleapis.com/customsearch/v1?'
        parameters = {'q': query.encode('UTF-8'),
                      'cx' : self.engine_id,
                      'key': self.api_key}
        if userip:
            parameters['userip'] = userip
        url += urlencode(parameters)
        json = get_json(url)
        return json

    def yt_search(self, query, num=1):
        url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&order=relevance&type=video&maxResults=5&'
        url += urlencode({'q': query.encode('UTF-8'),
                          'key': self.api_key})
        js = get_json(url)
        if not js['items']:
            return None

        video_info = js['items'][0]
        url = 'https://www.googleapis.com/youtube/v3/videos?&part=contentDetails,statistics&'
        url += urlencode({'id': video_info['id']['videoId'],
                          'key': self.api_key})
        js = get_json(url)
        video_details = js['items'][0]
        m = re.search(r'P((?:\d)+D)?T?(\d+H)?(\d+M)?(\d+S)?',
                      video_details['contentDetails']['duration'])
        days, hours, minutes, seconds = map(lambda x: int(x[:-1]) if x else 0, m.groups())
        stats = video_details['statistics']

        return {
            'title': video_info['snippet']['title'],
            'duration': 86400 * days + 3600 * hours + 60 * minutes + seconds,
            'uploaded': video_info['snippet']['publishedAt'],
            'id': video_info['id']['videoId'],
            'favorite_count': int(stats['favoriteCount']),
            'view_count': int(stats['viewCount']),
            'liked': int(stats['likeCount']),
            'disliked': int(stats['dislikeCount'])
        }
