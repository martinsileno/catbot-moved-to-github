import io
import json
from urllib.request import urlopen
from urllib.parse import quote_plus

from modules.module import Module


class mod_Imdb(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.apikey = config.get('imdb', 'key')
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}
        self.battles = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'imdb':
            url = f'http://www.omdbapi.com/?apikey={self.apikey}&i=&t={quote_plus(arg)}'
            reply = self.get_json(url)
            response = reply['Response']
            if response != 'True':
                self.msg(channel, 'error bla bla not found blabla')
                return

            self.msg(channel,
                     '@sep @b{r[Title]}@b [{r[Year]}] Rated {r[Rated]} @sep @bRating@b '
                     '{r[imdbRating]}/10, {r[imdbVotes]} votes @sep @bGenre@b {r[Genre]} @sep '
                     '@bDirector@b {r[Director]} @sep @bActors@b {r[Actors]} @sep '
                     '@bRuntime@b {r[Runtime]} @sep'.format(r=reply))
            self.msg(channel,
                     '@sep @bPlot@b {r[Plot]} @sep @uhttp://www.imdb.com/title/{r[imdbID]}/@u @sep'
                     .format(r=reply))

    def get_json(self, url):
        u = urlopen(url)
        cont = u.read()
        page = json.load(io.StringIO(cont.decode()))
        return page

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
