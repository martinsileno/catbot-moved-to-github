from urllib.request import urlopen
from urllib.parse import urlencode
from lxml import etree

from modules.feed import XmlFeed
from modules.module import Module


class mod_Wolfram(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.key = self.config.get('wolfram', 'key')
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] not in '!.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        cmd = cmd.lower()
        if cmd == 'wolfram':
            url = 'http://api.wolframalpha.com/v2/query?appid={key}&format=plaintext&'.format(
                key=self.key)
            url += urlencode({'input': arg.encode('UTF-8')})
            x = urlopen(url).read()
            f = etree.fromstring(x)
            xml = XmlFeed(f)
            lines = 1
            for sec in xml.elements('/queryresult/pod'):
                title = sec.text('@title')
                for line in sec.text('subpod/plaintext').split('\n'):
                    self.msg(channel, '@b{title}@b: {text}'.format(title=title, text=line))
                    lines += 1
                    if lines == 10:
                        break
                if lines == 10 or title == 'Result':
                    break

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
