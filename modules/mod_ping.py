import time

from modules.module import Module


class mod_Ping(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.pending = {}
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False},
                      'ctcpreply_ping': {'name': self.on_CTCPREPLY_ping, 'thread': False}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!':
            return

        if args == '!ping':
            if sender.nick in self.pending:
                return

            ts = int(time.time() * 1000)
            self.bot.ping(sender.nick)
            self.pending[sender.nick] = {'channel': channel, 'ts': ts}

    def on_CTCPREPLY_ping(self, sender, target, arg):
        if sender.nick in self.pending:
            p = self.pending[sender.nick]
            diff = int(time.time() * 1000) - p['ts']
            self.msg(p['channel'], '[ping] {0}: ping reply: {1}ms'.format(sender.nick, diff))
            del self.pending[sender.nick]

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
