import random
import time

from modules.module import Module


class mod_Rejoin(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'kick': {'name': self.on_kick, 'thread': True}}
        self.commands = {}

    def start(self):
        self.active = True
        return True

    def on_kick(self, sender, target, channel, reason):
        if target == self.bot.nick:
            time.sleep(random.randint(1, 5))
            self.bot.sender.raw_line('ChanServ UNBAN %s' % channel)
            self.bot.join(channel)
            self.msg(channel, 'wat u kick %s' % sender.nick)

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
