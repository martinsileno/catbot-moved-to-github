import io
import logging
import re
from datetime import datetime
from urllib.request import urlopen

import lxml.html
import requests
from imgurpython import ImgurClient
from requests.exceptions import RequestException

from modules.feed import HtmlFeed, PartialHtmlFeed, get_json
from modules.module import Module

logger = logging.getLogger(__name__)


class mod_Pagetitle(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.webre = re.compile(
            r'(https?://(?:www\.)?((?:[A-Za-z0-9\-]+\.)?[a-zA-Z0-9\-]+?)\.[^\s]+)')
        self.imgur_regex = re.compile(
            r'(https?://)?(www\.)?(i\.)?imgur\.com(/gallery)?/(?P<img_id>[a-zA-Z0-9]+)(\.[a-zA-Z]+)?')
        self.replagex = re.compile(r'\s+')
        self.websites = {
            'i.imgur': self.get_imgur_pagetitle,
            'gfycat': self.get_gfycat_title,
            'store.steampowered': self.get_generic_pagetitle,
            'streamable': self.get_streamable_pagetitle,
            'japan-guide': self.get_japan_guide_pagetitle,
        }

        client_id = config.get('imgur', 'client_id')
        client_secret = config.get('imgur', 'client_secret')
        self.imgur_client = ImgurClient(client_id, client_secret)

        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        known = False
        for url, website in self.webre.findall(args):
            if website in self.websites:
                known = True
                title = self.websites[website](url)
                if title:
                    if website == 'japan-guide':
                        self.msg(channel, title)
                    else:
                        self.msg(
                            channel,
                            '[title] @b{}@b - @c15{}@c'.format(url, title)
                        )

            if not known:
                meta = self.get_ogp_meta(url)
                if meta is not None and 'title' in meta and 'description' in meta:
                    if 'site_name' in meta:
                        site_name = meta['site_name']
                        site_str = '@c{}{}@c '.format(
                            self.get_section_color(site_name), site_name)
                    else:
                        site_str = ''
                    meta['description'] = meta['description'][:400]
                    self.msg(channel, '{site}@b{m[title]}@b: {m[description]}'.format(
                        site=site_str, m=meta))

    def get_ogp_meta(self, url):
        try:
            r = PartialHtmlFeed(url)
            t = r.get_tree()
            charset = t.xpath('//meta/@charset')
            if charset:
                r.response.encoding = charset[0]
                t = r.get_tree()

            q = t.xpath('//meta[starts-with(@property, "og:")]')
            meta = {}
            for m in q:
                attribs = m.attrib
                meta[attribs['property'][3:]] = attribs['content']
        except RequestException:
            logger.exception('Error getting pagetitle for %s' % url)
            meta = None

        return meta

    def get_generic_pagetitle(self, url):
        u = urlopen(url)
        cont = u.read()
        page = lxml.html.parse(io.StringIO(cont.decode()))
        title = page.find('/head/title')
        if title is not None:
            title = title.text
        else:
            return None
        title = title.replace('\n', '').replace('\t', '')
        title = re.sub(self.replagex, ' ', title)
        return title.strip()

    def get_streamable_pagetitle(self, url):
        u = urlopen(url)
        cont = u.read()
        page = lxml.html.parse(io.StringIO(cont.decode()))
        title = page.find('/head/title')
        if title is not None:
            title = title.text
            title = title.replace('\n', '').replace('\t', '')
            title = re.sub(self.replagex, ' ', title).strip()
        if not title or title == 'Streamable - simple video sharing':
            titles = page.xpath('/html/body/div[3]/div[1]/div[2]/div[1]/div[1]/h1')
            if titles:
                title = titles[0].text
            else:
                return title
            title = title.replace('\n', '').replace('\t', '')
            title = re.sub(self.replagex, ' ', title)
            return title.strip()
        else:
            return title

    def get_imgur_pagetitle(self, url):
        m = self.imgur_regex.match(url)
        img_id = m.group('img_id')
        img = self.imgur_client.get_image(img_id)

        if not img.title:
            ls, _ = img.link.rsplit('.', 1)
            base_url = ls.replace('//i.', '//')
            t = HtmlFeed(base_url).get_tree()
            try:
                img.title = t.xpath('/html/head/title')[0].text.strip()
            except:
                try:
                    img.title = t.xpath('/html/body/div[8]/div[1]/div[1]/div[2]/h1')[0].text + ' *'
                except Exception:
                    logger.exception('Could not get page title for %s', base_url)

        title = '{section}@b{title}@b{nsfw} - {width} x {height}, {size:,} KB - {views:,} views - {date}'.format(
            section='@c%d[/%s]@c15 ' % (self.get_section_color(img.section), img.section) if img.section else '',
            title=img.title if img.title else 'No title',
            nsfw=' @c4[NSFW]@c15' if img.nsfw else '',
            width=img.width,
            height=img.height,
            size=round(img.size/1024, 2),
            views=img.views,
            date=datetime.fromtimestamp(img.datetime).strftime('%Y-%m-%d %H:%M:%S')
        )
        return title

    def get_gfycat_title(self, url):
        _, img_name = url.rsplit('/', 1)
        img_name = img_name.rstrip('#')
        img = get_json('http://gfycat.com/cajax/get/%s' % img_name)['gfyItem']

        img_title = 'No title'
        if img['redditIdText']:
            img_title = img['redditIdText']
        if img['title']:
            img_title = img['title']

        title = '{section}@b{title}@b{nsfw} - {width} x {height} - {fps} fps - {size:,} KB - {views:,} views - {date}'.format(
            section='@c%d[/%s]@c15 ' % (self.get_section_color(img['subreddit']), img['subreddit']) if img['subreddit'] else '',
            title=img_title,
            nsfw=' @c4[NSFW]@c15' if img['nsfw'] and img['nsfw'] != '0' else '',
            width=img['width'],
            height=img['height'],
            fps=img['frameRate'],
            size=round(int(img['webmSize'])/1024.0, 2),
            views=img['views'],
            date=datetime.fromtimestamp(int(img['createDate'])).strftime('%Y-%m-%d %H:%M:%S')
        )
        return title

    def get_japan_guide_pagetitle(self, url):
        cont = requests.get(url).text
        tree = lxml.html.parse(io.StringIO(cont))
        html_title = tree.xpath('/html/head/title')[0].text
        html_desc = tree.xpath('/html/head/meta[@name="description"]/@content')[0]
        rating = tree.xpath(
            "/html/body//div[@class='section_header__title']//span[@data-dots]")
        if rating:
            rating_dots = f' @c4@b{rating[0].text}@o'
        else:
            rating_dots = ''
        title = f'@c4japan-guide.com@c @b{html_title}@b{rating_dots}@o ' \
                f'{html_desc}'
        return title

    def get_section_color(self, name):
        if not name:
            return None
        enabled_colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15]
        return enabled_colors[sum(ord(c) for c in name) % len(enabled_colors)]

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
