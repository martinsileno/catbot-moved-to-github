import requests
from requests.exceptions import RequestException

from modules.module import Module
from modules.mod_google import Google


COUNTRIES = {
    'china': 'Chinese',
    'czech': 'Czechoslovakian',
    'france': 'French',
    'germany': 'German',
    'japan': 'Japanese',
    'sweden': 'Swedish',
    'uk': 'British',
    'usa': 'American',
    'ussr': 'Soviet',
}

TYPES = {
    'heavy': 'heavy tank',
    'light': 'light tank',
    'medium': 'medium tank',
    'spg': 'self-propelled gun',
    'td': 'tank destroyer',
}


class mod_Wot(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.loaded_data = False
        self.tank_list = None
        self.tank_cache = {}
        self.mission_list = None
        key = config.get('google', 'key')
        engine_id = config.get('google', 'engine_id')
        self.google = Google(key, engine_id)
        self.google_cache = {}
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '.' and args[0] != '!':
            return

        cmd, sep, args = args[1:].partition(' ')
        if cmd.lower() == 'tank':
            self.cmd_tank(channel, args)

    def cmd_tank(self, channel, args):
        if not self.loaded_data:
            self.load_data()
            self.loaded_data = True

        query = args.lower()
        found_tanks = self.local_search(query)
        if not found_tanks:
            try:
                found_tanks = self.fallback_search(query)
            except:
                pass
            if not found_tanks:
                self.errormsg(channel, 'no tanks found matching {}'.format(args))
        elif len(found_tanks) > 1:
            self.errormsg(channel, 'multiple tanks matching {} found: {}'.format(
                args, ', '.join(t['name'] for t in found_tanks)))
            return

        t = found_tanks[0]
        is_premium = t['goldCost'] != 0
        if is_premium and t['cost'] != 0:
            self.errormsg(channel, 'found tank that has gold cost and silver cost:')

        self.msg(channel,
                 '@sep @b{t[name]}{premium}@b {country} tier {t[tier]} {type} ({weight}t) '
                 '@sep @bHull armor@b {t[hullArmorFront]}/{t[hullArmorSide]}/{t[hullArmorRear]}mm '
                 '@sep @bTurret armor@b {t[turretArmorFront]}/{t[turretArmorSide]}/{t[turretArmorRear]}mm '
                 '@sep @bGun@b {t[gunName]} {t[penetration]}p/{t[damage]}d, -{t[depression]}/+{t[elevation]}deg, reloads in {t[reloadTime]}s '
                 '@sep @bSpeed@b {t[forwardSpeed]} / {t[reverseSpeed]} km/h @sep'.format(
                     t=t,
                     weight=round(t['totalWeight'] / 1000, 1),
                     premium='@c7*@c' if is_premium else '',
                     country=COUNTRIES[t['nation']],
                     type=TYPES[t['type']],
                 ))

    def local_search(self, query):
        found_tanks = []
        for tank in self.tank_list:
            if '_IGR' in tank['identifier']:
                continue
            if query == tank['name'].lower():
                found_tanks = [tank]
                break
            elif query in tank['name'].lower() or query in tank['shortName']:
                found_tanks.append(tank)
        return found_tanks

    def fallback_search(self, query):
        if query.lower() in self.google_cache:
            return self.google_cache[query.lower()]

        try:
            result = self.google.search('%s wot' % query)
        except RequestException:
            return
        if result['queries']['request'][0]['totalResults'] == 0:
            return

        json = result['items'][0]
        title = json['title']
        if title.endswith('Global wiki. Wargaming.net'):
            tankname, _ = title.split(' - ')
            result = self.local_search(tankname.lower())
            self.google_cache[query.lower()] = result
            return result

    def load_data(self):
        seen = set()
        self.tank_list = []
        for tier in range(1, 11):
            for tank in requests\
                    .get('http://old.tanks.gg/api/stats/%d/light,medium,heavy,td,spg/silver/dpm' % tier)\
                    .json():
                if tank['identifier'] not in seen and '_IGR' not in tank['identifier']:
                    self.tank_list.append(tank)
                    seen.add(tank['identifier'])
        self.mission_list = requests.get('http://tanks.gg/api/missions').json()

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
