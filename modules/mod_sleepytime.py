from datetime import datetime, timedelta

from modules.module import Module


class mod_Sleepytime(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'bt':
            time_to_sleep = 14
            now = datetime.now()
            now += timedelta(minutes=time_to_sleep)
            colors = [5, 4, 7, 8, 9, 3]
            wakeup_times = []
            wakeup_time = now
            for _ in colors:
                wakeup_time += timedelta(minutes=90)
                wakeup_times.append(wakeup_time)

            s = '@sep @bBedTime@b now @sep @bFalling asleep@b in 14 minutes @sep ' \
                '@bWake up@b at {} @sep'.format(
                    ' - '.join(['@c{:02d}{}@c'.format(color, wakeup_time.strftime('%H:%M'))
                                for color, wakeup_time in zip(colors, wakeup_times)]))
            self.msg(channel, s)

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
