class Module(object):
    def __init__(self, bot, config):
        self.active = False
        self.bot = bot
        self.config = config
        self.db = bot.db
        self.version = 0
        self.version_minor = 0

        self.hooks = {}
        self.commands = {}

    def reload(self):
        self.config.read('config.ini')

    def shutdown(self):
        if not self.active:
            return

    def start(self):
        pass

    def msg(self, target, msg):
        self.bot.privmsg(target,
                         msg.replace('@errsep', '@b@c4::@o')
                         .replace('@nsep', '@b@c7::@o')
                         .replace('@sep', '@b@c3::@o')
                         .replace('@b', chr(2))
                         .replace('@c', chr(3))
                         .replace('@o', chr(15))
                         .replace('@u', chr(31))
                         .replace('RocketNews', 'RacketNews')
                         .replace('rocketnews', 'rocketnews'))


    def errormsg(self, target, message):
        self.msg(target, '@b@c4Error:@o %s' % message)


def requireflag(func, flag):
    def wrapper(self, sender, channel, args):
        if self.bot.is_master(sender) or flag in self.bot.flags.get(sender.nick, ''):
            func(self, sender, channel, args)
    return wrapper

