def format_thousand(number, add_prefix=False):
    if not isinstance(number, int):
        return str(number)

    text = str(abs(number))

    length = len(text)
    count = int((length - 1) / 3)

    for i in range(1, count + 1):
        text = text[:length - (i * 3)] + ',' + text[length - (i * 3):]

    if number < 0:
        text = '-' + text
    elif add_prefix:
        text = '+' + text

    return text


def format_hms(seconds):
    hours = int(seconds / 3600)
    seconds -= 3600 * hours
    minutes = int(seconds / 60)
    seconds -= 60 * minutes
    if hours == 0:
        return "%02d:%02d" % (minutes, seconds)

    return "%02d:%02d:%02d" % (hours, minutes, seconds)
