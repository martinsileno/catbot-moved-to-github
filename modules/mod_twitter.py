import logging
import threading
import time
from datetime import datetime

from twitter import Twitter, TwitterError, OAuth

from modules.module import Module

logger = logging.getLogger(__name__)


class mod_Twitter(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.db.query('CREATE TABLE IF NOT EXISTS twitter_friends '
                      '(id INT PRIMARY KEY, '
                      'name TEXT NOT NULL COLLATE NOCASE, '
                      'added_by TEXT NOT NULL COLLATE NOCASE, '
                      'date_added DATETIME NOT NULL)')
        self.channel = self.config.get('twitter', 'channel')
        self.db.query('SELECT * FROM twitter_friends')
        self.friends = [{'name': row[1], 'id': row[0], 'last_check': None}
                        for row in self.db.fetchall()]
        self.twitter = Twitter(auth=OAuth(self.config.get('twitter', 'oauth_token'),
                                          self.config.get('twitter', 'oauth_secret'),
                                          self.config.get('twitter', 'consumer_key'),
                                          self.config.get('twitter', 'consumer_secret')))
        self.last_tweet_id = 0
        self.my_loop = threading.Thread(target=self.twitter_monitor)
        self.monitor = True
        self.my_loop.start()

    def on_privmsg(self, sender, channel, args):
        if not args.startswith('!'):
            return

        cmd, sep, args = args[1:].partition(' ')
        cmd = cmd.lower()

        if cmd == 'follow':
            self.follow(sender, channel, args)
        elif cmd == 'unfollow':
            self.unfollow(sender, channel, args)
        elif cmd == 'following':
            self.msg(channel, '@bFollowing@b: {flist}'.format(
                flist=', '.join(x['name'] for x in self.friends)))

    def follow(self, sender, channel, target):
        for friend in self.friends:
            if friend['name'] == target:
                self.msg(channel, "I'm already following @b%s@b" % target)
                return

        try:
            user = self.twitter.friendships.create(screen_name=target)
            self.db.query(
                'INSERT INTO twitter_friends (id, name, added_by, date_added) VALUES (?, ?, ?, ?)',
                user['id'], user['screen_name'], sender.nick, datetime.now())
            self.friends.append({'name': user['screen_name'], 'id': user['id'], 'last_check': None})
            self.msg(channel, 'Now following @b%s@b' % target)
        except TwitterError:
            self.msg(channel, '@bError@b: something went wrong')
            logger.exception('Exception when trying to follow %s', target)

    def unfollow(self, sender, channel, target):
        found = False
        for friend in self.friends:
            if friend['name'] == target:
                found = True
                break

        if not found:
            self.msg(channel, "I'm not following @b%s@b" % target)
            return

        try:
            self.twitter.friendships.destroy(screen_name=target)
            self.db.query('DELETE FROM twitter_friends WHERE name = ?', target)
            for friend in self.friends:
                if friend['name'] == target:
                    self.friends.remove(friend)
                    break

            self.msg(channel, 'No longer following @b%s@b' % target)
        except TwitterError:
            self.msg(channel, '@bError@b: something went wrong')
            logger.exception('Exception when trying to unfollow %s', target)

    def check_tweets(self):
        try:
            if self.last_tweet_id == 0:
                tweets = self.twitter.statuses.home_timeline()
                self.last_tweet_id = tweets[0]['id']
            else:
                tweets = reversed(self.twitter.statuses.home_timeline(since_id=self.last_tweet_id))
                for tweet in tweets:
                    text = tweet['text'].replace('\n', ' - ')
                    for url in tweet['entities']['urls']:
                        text = text.replace(url['url'], url['expanded_url'])

                    tweet_url = 'https://twitter.com/%s/status/%s' % \
                                (tweet['user']['screen_name'], tweet['id_str'])
                    self.msg(self.channel,
                             '@b[twitter] @c{c}{t[user][screen_name]}@c@b {text} @c15{url}'.format(
                                 t=tweet,
                                 text=text,
                                 url=tweet_url,
                                 c=self.get_user_color(tweet['user']['screen_name'])))
                    self.last_tweet_id = tweet['id']
        except Exception:
            logger.exception('Exception when checking tweets')

    def twitter_monitor(self):
        while self.monitor:
            self.check_tweets()
            time.sleep(80)

    def get_user_color(self, name):
        enabled_colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15]
        return enabled_colors[sum(ord(c) for c in name) % len(enabled_colors)]

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.monitor = False

        self.active = False
        return True
