from urllib.parse import urlencode

from requests.exceptions import RequestException

from modules.feed import BrokenXmlFeed
from modules.module import Module


class mod_Ipinfo(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.ipinfo = IpInfo(config.get('ipinfodb', 'key'))
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        cmd, sep, arg = args.partition(' ')
        if not (cmd.lower() == '!ipinfo' or cmd.lower() == '.ipinfo'):
            return
        try:
            reply = self.ipinfo.get_info(arg)
        except RequestException as e:
            self.errormsg(channel, e)
            return

        self.msg(channel,
                 '@sep @bIP/Host@b {arg} ({reply[ip_addr]}) @sep '
                 '@bLocation@b {reply[city]}, {reply[region]}, {reply[country_name]} '
                 '[{reply[country_code]}] @sep{map}'.format(
                     reply=reply,
                     arg=arg.lower(),
                     map=' http://maps.google.com/maps?q=%s,%s @sep' %
                         (reply['latitude'],
                          reply['longitude']) if reply['latitude'] and reply['longitude'] else ''))

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True


class IpInfo(object):
    def __init__(self, api_key):
        self.api_key = api_key
        self.cache = {}  # todo

    def get_info(self, target):
        url = 'http://api.ipinfodb.com/v3/ip-city/?key=%s&format=xml&' % self.api_key
        url += urlencode({'ip': target})
        reply = BrokenXmlFeed(url)
        r = reply.elements('/Response')[0]
        return {
            'ip_addr': r.text('ipAddress'),
            'status': r.text('statusMessage'),
            'country_code': r.text('countryCode', 'N/A'),
            'country_name': r.text('countryName', 'N/A').capitalize(),
            'region': r.text('regionName', 'N/A').capitalize(),
            'city': r.text('cityName', 'N/A').capitalize(),
            'zip': r.text('zipCode', 'N/A'),
            'latitude': r.text('latitude'),
            'longitude': r.text('longitude'),
            'timezone': r.text('timezone', 'N/A')
        }
