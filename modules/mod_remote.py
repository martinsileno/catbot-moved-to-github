import json
import logging
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer

from modules.module import Module

logger = logging.getLogger(__name__)


class mod_Remote(Module):

    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {}
        self.channel = config.get('remote', 'channel')
        self.port = int(config.get('remote', 'port'))
        self.ip_whitelist = [s.strip() for s in config.get('remote', 'ip_whitelist').split(',')]
        self.commands = {}
        self.seen_data = []
        self.server = None

    def start(self):
        self.active = True
        self.server = JSONRPCServer(('', self.port), JSONRPCServerHandler, self.ip_whitelist,
                                    JSONRPCSuccessHandler(self), JSONRPCErrorHandler(self))
        t = threading.Thread(target=self.server.serve_forever)
        logger.info('Started JSON RPC Server')
        t.start()
        return True

    def shutdown(self):
        if not self.active:
            return

        self.server.shutdown()
        self.server.socket.close()
        logger.info('Shutdown JSON RPC Server')
        self.active = False
        return True


class JSONRPCHandler(object):

    def __init__(self, bot):
        self.bot = bot
        self.msg = self.bot.msg
        self.errormsg = self.bot.errormsg
        self.channel = self.bot.channel


class JSONRPCSuccessHandler(JSONRPCHandler):

    def send(self, message):
        self.msg(self.channel, message)

    def __call__(self, method, params):
        getattr(self,  method)(*params)


class JSONRPCErrorHandler(JSONRPCHandler):

    def __call__(self, error_msg):
        self.errormsg(self.channel, '[HTTP SERVER] - %s' % error_msg)


class JSONRPCServer(HTTPServer):

    def __init__(self, addr, req_handler, ip_whitelist, rpc_handler, exc_handler):
        super().__init__(addr, req_handler)
        self.ip_whitelist = ip_whitelist
        self.rpc_handler = rpc_handler
        self.exc_handler = exc_handler


class JSONRPCServerHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        source_ip = ''
        data = ''
        try:
            source_ip, _ = self.client_address
            if source_ip not in self.server.ip_whitelist:
                logger.warning('[JSON RPC Server] Denied request from %s', source_ip)
                self.server.exc_handler('[JSON RPC Server] Denied request from %s' % source_ip)
                self.send_response(403)
                self.end_headers()
                return

            length = int(self.headers['Content-Length'])
            data = self.rfile.read(length).decode('utf-8')
            logger.info('[JSON RPC Server] <<< %s', data)
            json_data = json.loads(data)
            self.server.rpc_handler(json_data['method'], json_data['params'])
            self.send_response(200)
            msg = {'status': 'OK', 'data': {}}
            enc_msg = str(msg).encode('UTF-8')
            self.send_header('Content-Length', len(enc_msg) + 2)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(b'%s\n\n' % enc_msg)
        except Exception as exc:
            logger.exception('Exception parsing POST request from %s with data: %s',
                             source_ip, data)
            self.send_response(500)
            self.end_headers()
            self.server.exc_handler('Exception %s' % exc)
