import hashlib
import logging
import time
from datetime import datetime, timedelta

import requests

from modules.module import Module

logger = logging.getLogger(__name__)
ATAF_ENDPOINT = 'http://www.temporealeataf.it/Mixer/Rest/PublicTransportService.svc/schedule'


class mod_Ataf(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.lat = config.get('ataf', 'lat')
        self.lon = config.get('ataf', 'lon')
        self.client_ticket = config.get('ataf', 'client_ticket')
        self.timezone = config.get('ataf', 'timezone', fallback='+2')
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}

    def on_privmsg(self, sender, channel, args):
        if not args.startswith('!') and not args.startswith('.'):
            return

        cmd, sep, args = args[1:].partition(' ')
        if cmd != 'ataf':
            return

        # TODO: per user aliases
        if args == 'home':
            self.parse_stop_info(channel, self.get_timetable('FM0106'))  # ospedale torre galli
        elif args == 'return':
            self.parse_stop_info(channel, self.get_timetable('FM0586'))  # petrarca
            self.parse_stop_info(channel, self.get_timetable('FM0511'))  # calza 02
        elif args == '2nd':
            self.parse_stop_info(channel, self.get_timetable('FM1529'))  # ariosto aleardi
            self.parse_stop_info(channel, self.get_timetable('FM0524'))  # sanzio

    def parse_stop_info(self, channel, resp):
        for elem in resp:
            today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
            eta = today + timedelta(hours=1, milliseconds=int(elem['d']))
            self.msg(channel, '[{stop}] Line {line} @sep To {direction} @sep {eta}'.format(
                stop='?',
                line=elem['n'],
                direction=elem['t'],
                eta='{:%H:%M}'.format(eta),
            ))

    def get_timetable(self, stop_code):
        payload = {
            "nodeID": stop_code,
            "lat": "43.787796666666665",
            "lon": "11.249808333333334",
            "timezone": "+2",
            "s": mod_Ataf.get_dynamic_password(self.client_ticket),
            "_": 0
        }

        r = requests.get(ATAF_ENDPOINT, params=payload, timeout=30)
        return r.json()

    @staticmethod
    def get_dynamic_password(s):
        s = str(mod_Ataf.calculate_normalized_time_from_begin()) + s
        return hashlib.sha1(s.encode('utf-8')).hexdigest()

    @staticmethod
    def calculate_normalized_time_from_begin():
        a = time.time() / 900
        return round(a) * 900
