import re
import random
from time import time

from modules.module import Module


class mod_Rng(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': False}}
        self.commands = {}
        self.dice_regex = re.compile('^(?:(\d+)d)?(\d+)(?:([\+\-])(\d+))?$')
        self.rnd = random.Random(int(time()))

    def on_privmsg(self, sender, channel, args):
        if args[0] != '!' and args[0] != '.':
            return

        cmd, sep, arg = args[1:].partition(' ')
        if cmd.lower() == 'd':
            self.cmd_dice(channel, arg)
        elif cmd.lower() in ['choice', 'choose']:
            self.cmd_choose(sender, channel, arg)

    def cmd_dice(self, channel, arg):
        r = self.dice_regex.search(arg)
        if not r:
            self.errormsg(channel, 'invalid format')
            return

        num, faces, type, modifier = r.groups()
        if num:
            num = int(num)
        else:
            num = 1
        faces = int(faces)
        if num < 1 or num > 32 or faces < 2 or faces > 65536:
            self.errormsg(channel, 'parameter out of range')
            return

        total = 0
        results = []
        for n in range(int(num)):
            randnum = self.rnd.randint(1, int(faces))
            total += randnum
            results.append(randnum)

        if type == '-':
            modifier = int(modifier)
            total -= modifier
            max = num * faces - modifier
        elif type == '+':
            modifier = int(modifier)
            total += modifier
            max = num * faces + modifier
        else:
            max = num * faces

        self.msg(channel,
                 '@sep @bTotal@b {total} / {max} [{percent:.2f}%] @sep @bResults@b {results} @sep'
                 .format(
                     total=total,
                     max=max,
                     percent=100 * total / max if max != 0 else '9001',
                     results=str(results)))

    def cmd_choose(self, sender, channel, arg):
        choices = arg.split()
        if choices:
            self.msg(channel, '@b{sender.nick}@b: {choice}'.format(
                sender=sender, choice=random.choice(choices)))
        else:
            self.errormsg(channel, 'cannot choose from an empty sequence')

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
