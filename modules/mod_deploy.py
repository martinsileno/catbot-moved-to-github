import logging

import git
import pip

from modules.module import Module

logger = logging.getLogger(__name__)


class mod_Deploy(Module):
    def __init__(self, bot, config):
        Module.__init__(self, bot, config)
        repo_path = self.config.get('deploy', 'path')
        self.repo = git.Repo(repo_path)
        self.hooks = {'privmsg': {'name': self.on_privmsg, 'thread': True}}
        self.commands = {}

    def on_privmsg(self, sender, channel, args):
        if args == '!deploy':
            pull = self.repo.git.pull()
            if pull == 'Already up-to-date.':
                self.msg(channel, pull)
                return

            output = pull.split('\n')
            if output[0].startswith('Updating'):
                self.msg(channel, output[0])
                self.msg(channel, output[-1])
                for upd_file in output[2:-1]:
                    upd_file = upd_file.strip()
                    if 'requirements.txt' in upd_file:
                        self.msg(channel, 'Updating packages using pip')
                        pip.main(['install', '-r', 'requirements.txt'])
                for upd_file in output[2:-1]:
                    upd_file = upd_file.strip()
                    if upd_file.startswith('modules/'):
                        mod_name = upd_file.split()[0]
                        mod_name = mod_name.split('/')[1]
                        mod_name = mod_name.split('_')[1]
                        mod_name = mod_name.replace('.py', '')
                        self.msg(channel, 'Reloading updated module %s' % mod_name)
                        self.bot.reload_module(mod_name)
                        self.msg(channel, 'Reloaded updated module %s' % mod_name)
                self.msg(channel, 'Done updating')
            else:
                self.msg(channel, 'Unknown event after git pull')
                logger.warning('Unknown event after git pull: %s', output)

    def start(self):
        self.active = True
        return True

    def shutdown(self):
        if not self.active:
            return

        self.active = False
        return True
