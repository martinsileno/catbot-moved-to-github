import sqlite3
import threading


class Database(object):
    def __init__(self, filename):
        self.conn = sqlite3.connect(filename, check_same_thread=False)
        self.cur = self.conn.cursor()
        self._lock = threading.Lock()

    def query(self, query, *args):
        with self._lock:
            self.cur.execute(query, args)
            self.conn.commit()

    def fetchone(self):
        with self._lock:
            return self.cur.fetchone()

    def fetchall(self):
        with self._lock:
            return self.cur.fetchall()

    @property
    def lastrowid(self):
        return self.cur.lastrowid
